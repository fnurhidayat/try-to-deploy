const express = require('express');
const app = express();
const PORT = process.env.PORT || 8000;
const { Task } = require('./models');

app.use(express.json());

app.get('/', (req, res) => {
  res.status(200).json({
    status: 'success',
    message: "Hello World"
  })
});

app.get('/tasks', async (req, res) => {
  res.status(200).json({
    status: 'success',
    data: {
      tasks: await Task.findAll()
    }
  })
})

app.post('/tasks', async (req, res) => {
  try {
    res.status(201).json({
      status: 'success',
      data: {
        task: await Task.create(req.body)
      }
    })
  }

  catch(err) {
    res.status(400).json({
      status: 'fail',
      errors: [err.message]
    })
  } 
})

app.listen(PORT, () => {
  console.log(`Server started at ${Date()}`);
  console.log(`Listening on port ${PORT}`);
}) 